package com.waykichain.controller

import com.waykichain.api.MempoolApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.context.request.NativeWebRequest
import java.util.*

@Controller
class MempoolApiController @Autowired constructor(private val request: NativeWebRequest)  {
     fun getRequest(): Optional<NativeWebRequest> {
        return Optional.ofNullable(request)
    }
}