package com.waykichain.controller

import com.alibaba.fastjson.JSON
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.collect.Lists
import com.waykichain.api.ApiUtil
import com.waykichain.coin.wicc.vo.WiccBlockResult
import com.waykichain.coin.wicc.vo.tx.UCoinTransferTx
import com.waykichain.config.Constant
import com.waykichain.model.*
import com.waykichain.model.Currency
import com.waykichain.service.ChainXService
import io.swagger.annotations.ApiParam
import org.apache.commons.lang3.tuple.Pair
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.request.NativeWebRequest
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import javax.annotation.PostConstruct
import javax.validation.Valid
import kotlin.collections.ArrayList

@Controller
class BlockApiController @Autowired constructor(private val request: NativeWebRequest) {
    @Autowired
    private val chainXService: ChainXService? = null
    private var genesisBlock: Block? = null

    @PostConstruct
    fun initGenesis() {
        val block = Block()
        val blockBean = chainXService!!.findBlockBean(0).block
        val blockIdentifier = BlockIdentifier()
        blockIdentifier.hash(blockBean.curr_block_hash)
                .index(0.toLong())
        block.blockIdentifier = blockIdentifier
        block.parentBlockIdentifier = blockIdentifier
        block.timestamp = blockBean.time
        val txs = blockBean.tx
        var index = 0
        for (tx in txs) {
            //1.set transaction_identifier
            val transaction = Transaction()
                    .transactionIdentifier(TransactionIdentifier()
                            .hash(tx))

            //2.set operations
            val txDetail = chainXService.getTxDetail(tx)
            if (txDetail!!.confirmed_height != null) {
                transaction.addOperationsItem(Operation().type(txDetail.tx_type)
                        .status("SUCCESS")
                        .operationIdentifier(OperationIdentifier().index(index.toLong())))
            } else transaction.addOperationsItem(Operation().type(txDetail.tx_type)
                    .status("FAILED")
                    .operationIdentifier(OperationIdentifier().index(index.toLong())))

            index++
            block.addTransactionsItem(transaction)
        }
        genesisBlock = block
    }

    fun getRequest(): Optional<NativeWebRequest> {
        return Optional.ofNullable(request)
    }

    @RequestMapping(value = ["/block"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun block(@ApiParam(value = "", required = true) @RequestBody blockRequest: @Valid BlockRequest?): ResponseEntity<BlockResponse> {
        val statusCode = AtomicInteger(200)

        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val blockResponse = BlockResponse()
                    var returnString = ""
                    val mapper = ObjectMapper()
                    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    try {
                        val blockIndex = blockRequest!!.blockIdentifier.index
                        val blockHash = blockRequest.blockIdentifier.hash
                        val rstBlock = Block()
                        var wiccBlock: WiccBlockResult? = null
                        var wiccBlockParent: WiccBlockResult? = null
                        if (blockIndex != null &&
                                blockIndex > chainXService!!.getMaxBlockNumber()) {
                            throw Exception("request block index > solidified index")
                        }
                        if (blockIndex != null && blockIndex == 0L) {
                            blockResponse.block = genesisBlock
                        } else {
                            //1. get block
                            if (blockIndex != null) {
                                wiccBlock = chainXService!!.findBlockBean(blockIndex.toInt()).block
                            } else if (blockHash != null) {
                                wiccBlock = chainXService!!.getBlockByHash(blockHash)
                            } else {
                                //通过index  hash获取，若两者都无，是否获取最新块？
                            }
                            wiccBlockParent = if (wiccBlock != null && wiccBlock.height > 0) {
                                chainXService!!.getBlockByHash(wiccBlock.prev_block_hash)
                            } else {
                                wiccBlock
                            }

                            //2. set block info
                            rstBlock.blockIdentifier = BlockIdentifier()
                                    .index(wiccBlock!!.height.toLong())
                                    .hash(wiccBlock.curr_block_hash)
                            rstBlock.parentBlockIdentifier = BlockIdentifier()
                                    .index(wiccBlockParent!!.height.toLong())
                                    .hash(wiccBlockParent.curr_block_hash)
                            rstBlock.timestamp = wiccBlock.time

                            //3. set tx info
                            val rstTxs = Lists.newArrayList<Transaction>()
                            val txs = wiccBlock.tx as ArrayList<String>
                            /*
                            var index = 0
                            for (tx in txs)
                            */
                            for ((index, tx) in txs.withIndex()) {
                                //3.1 set tx
                                val rstTx = Transaction()
                                        .transactionIdentifier(TransactionIdentifier()
                                                .hash(tx))

                                //3.2 set operations
                                val txDetail = chainXService!!.getTxDetail(tx)

                                if (txDetail!!.confirmed_height != null) {
                                    if (txDetail.tx_type == "UCOIN_TRANSFER_TX") {
                                        val transferTx = txDetail as UCoinTransferTx
                                        val currency = Currency().decimals(8).symbol(transferTx.transfers[0].coin_symbol)

                                        rstTx.addOperationsItem(Operation()
                                                .operationIdentifier(OperationIdentifier().index(index.toLong()))
                                                .type(txDetail.tx_type)
                                                .amount(Amount().currency(currency).value(transferTx.transfers[0].coin_amount.toString()))
                                                .account(AccountIdentifier().address(transferTx.transfers[0].to_addr))
                                                .status("SUCCESS"))
                                    } else
                                        rstTx.addOperationsItem(Operation()
                                                .operationIdentifier(OperationIdentifier().index(index.toLong()))
                                                .type(txDetail.tx_type)
                                                .status("SUCCESS"))
                                } else {
                                    if (txDetail.tx_type == "UCOIN_TRANSFER_TX") {
                                        val transferTx = txDetail as UCoinTransferTx

                                        rstTx.addOperationsItem(Operation()
                                                .operationIdentifier(OperationIdentifier().index(index.toLong()))
                                                .type(txDetail.tx_type)
                                                .amount(Amount().value(transferTx.transfers[0].coin_amount.toString()))
                                                .account(AccountIdentifier().address(transferTx.transfers[0].to_addr))
                                                .status("FAILED"))
                                    } else
                                        rstTx.addOperationsItem(Operation()
                                                .operationIdentifier(OperationIdentifier().index(index.toLong()))
                                                .type(txDetail.tx_type)
                                                .status("FAILED"))
                                }
                                rstTxs.add(rstTx)
                            }
                            rstBlock.transactions = rstTxs
                            //4.response
                            blockResponse.block = rstBlock
                        }
                        returnString = mapper.writeValueAsString(blockResponse)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    ApiUtil.setExampleResponse(request, "application/json", returnString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode.get()))
    }


    @RequestMapping(value = ["/block/transaction"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun blockTransaction(
            @ApiParam(value = "", required = true) @RequestBody blockTransactionRequest: @Valid BlockTransactionRequest?): ResponseEntity<BlockTransactionResponse> {
        var statusCode = 200
        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val pair = handlerBlockTransaction(blockTransactionRequest)
                    statusCode = pair.left
                    ApiUtil.setExampleResponse(request, "application/json", pair.right)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode))
    }

    private fun handlerBlockTransaction(blockTransactionRequest: BlockTransactionRequest?): Pair<Int, String> {
        var returnString: String
        val blockTransactionResponse = BlockTransactionResponse()
        var error = Error()
        val mapper = ObjectMapper()
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        try {
            val blockIndex = blockTransactionRequest!!.blockIdentifier.index
            val txID = blockTransactionRequest.transactionIdentifier.hash
            println("blockIndex:$blockIndex")
            if (blockIndex > chainXService!!.getMaxBlockNumber()) {
                error = Constant.BLOCK_ID_OVER_CURRENT_LAST
                returnString = JSON.toJSONString(error)
                return Pair.of(400, returnString)
            }


            if (blockIndex == 0.toLong())
                blockTransactionResponse.transaction=handleBlockTrxHash(chainXService.findBlockBean(0).block.tx,txID)
            else{
                val txs = chainXService.findBlockBean(blockIndex.toInt()).block.tx
                blockTransactionResponse.transaction = handleBlockTrxHash(txs, txID)
            }
            returnString = mapper.writeValueAsString(blockTransactionResponse)
        } catch (e: Exception) {
            e.printStackTrace()
            error = Constant.SERVER_EXCEPTION_CATCH
            error.details = Error().message(e.message)
            returnString = JSON.toJSONString(error)
            return Pair.of(500, returnString)
        }
        return Pair.of(200, returnString)
    }


    private fun handleBlockTrxHash(txs: List<String>, txID: String): Transaction {
        val rstTx = Transaction()
        for ((index, tx) in txs.withIndex()) {
            if (tx == txID) {
                //3.1 set tx
                rstTx.transactionIdentifier(TransactionIdentifier()
                        .hash(tx))
                //3.2 set operations

                val txDetail = chainXService!!.getTxDetail(tx)

                if (txDetail!!.tx_type == "UCOIN_TRANSFER_TX") {
                    val transferTx = txDetail as UCoinTransferTx
                    val currency = Currency()
                    currency.decimals(8).symbol(transferTx.transfers[0].coin_symbol)
                    rstTx.addOperationsItem(Operation()
                            .operationIdentifier(OperationIdentifier().index(index.toLong()))
                            .type(transferTx.tx_type)
                            .amount(Amount().currency(currency).value(transferTx.transfers[0].coin_amount.toString()))
                            .account(AccountIdentifier().address(transferTx.transfers[0].to_addr))
                            .status("SUCCESS"))
                } else rstTx.addOperationsItem(Operation()
                        .operationIdentifier(OperationIdentifier().index(index.toLong()))
                        .type(txDetail.tx_type)
                        .status("SUCCESS"))
                break
            }
        }
        return rstTx
    }
}