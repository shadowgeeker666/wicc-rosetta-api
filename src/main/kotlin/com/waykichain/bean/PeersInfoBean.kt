package com.waykichain.bean

class PeersInfoBean {
    var addr: String? = null
    var services: String? = null
    var lastsend: Long? = null
    var lastrecv: Long? = null
    var bytessent: Long? = null
    var bytesrecv: Long? = null
    var conntime: Long? = null
    var pingtime: Long? = null
    var version: Long? = null
    var asubverddr: Long? = null
    var inbound: Boolean? = null
    var startingheight: Long? = null
    var abanscoreddr: Long? = null
    var syncnode: Boolean? = null
}